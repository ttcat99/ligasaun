﻿Imports System.Net
Imports System.IO
Module Module1

    Sub Main()
        Dim Listener As New System.Net.HttpListener
        Listener.Start()
        While True
            Dim context As HttpListenerContext = Listener.GetContext()
            Dim req As HttpListenerRequest = context.Request
            Dim reqURL As String = req.RawUrl.Replace("/", "\\")
            Dim Connect As New ServerThread(reqURL)
        End While
        Listener.Stop()
    End Sub

End Module


Class ServerThread
    Dim RecievedRequestURL As String = ""

    Public Sub New(ByVal RequestURL As String)
        RecievedRequestURL = RequestURL
        Dim t As New System.Threading.Thread(AddressOf ServerRun)
        t.Start()
    End Sub


    Private Sub ServerRun()
        Dim res As HttpListenerResponse = context.Response

        ' リクエストされたURLからファイルのパスを求める
        Dim path As String = req.RawUrl.Replace("/", "\\")

        ' ファイルが存在すればレスポンス・ストリームに書き出す
        If File.Exists(path) Then
            Dim content() As Byte = File.ReadAllBytes(path)
            res.OutputStream.Write(content, 0, content.Length)
        End If
        res.Close()
    End Sub

End Class
